
GJHIntervate = {};

function trace(msg)
	DEFAULT_CHAT_FRAME:AddMessage(msg);
end



function GJHIntervate.OnLoad(frame)
	frame:RegisterEvent("ADDON_LOADED");
	frame:SetScript("OnUpdate", GJHIntervate.onUpdate);
	
	SLASH_GJHIntervate1 = "/GJHIntervate";
	SLASH_GJHIntervate2 = "/GJHI";
	SlashCmdList["GJHIntervate"] = GJHIntervate.CLI;
	SlashCmdList["GJHI"] = GJHIntervate.CLI;
end

--
-- If GJHIntervate > 0 then there is a message that needs sent
-- We wait 2 seconds
--
function GJHIntervate.onUpdate(self, elapsed)
	local timeNow = time();
	if (GJHIntervate.timeCheck + 2 < timeNow) then 
		if (UnitClass("player") == "Druid") then
			GJHIntervate.timeCheck = timeNow;
			local mana =  UnitPower("player");
			local max = UnitPowerMax("player");
			local isDead =  UnitIsDeadOrGhost("player");
			local pct = mana/max;
			if (pct <= 0.8 and not isDead) then
				local start, duration, enable = GetSpellCooldown("Innervate");
				if (duration < 2.0 ) then
					trace("Innervate now idot....");
					PlaySoundFile("Interface\\AddOns\\GJHIntervate\\Sound\\dingding.mp3");
				end;
			end;
		else
			GJHIntervate.timeCheck = timeNow + 5000;
			
		end;
	end;

end;

function GJHIntervate.OnEvent(self,event,...)
	local arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11 = ...;
	if(event == "ADDON_LOADED") then
		if arg1 == "GJHIntervate" then
			
			if (GJHIntervate_Options == nil) then
				GJHIntervate_Options = {};
			end

			GJHIntervate.timeCheck = 0;
			trace("GJHIntervate is loaded");
		end
	end
end

function GJHIntervate.CLI(options)
	trace("GJHI");
end
